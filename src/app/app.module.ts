import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { BodyComponent } from './body/body.component';
import { FirstPageComponent } from './first-page/first-page.component';
import { SecondPageComponent } from './second-page/second-page.component';
import { AppRoutingModule } from './/app-routing.module';
import { CapitalizePipe } from './capitalize.pipe';
import { TranslatePipe } from './translate.pipe';
import { FirstPageComp1Component } from './first-page-comp1/first-page-comp1.component';
import { FirstPageComp2Component } from './first-page-comp2/first-page-comp2.component';
import { FirstPageComp3Component } from './first-page-comp3/first-page-comp3.component';
import { FirstPageComp4Component } from './first-page-comp4/first-page-comp4.component';
import { PanelstyleDirective } from './panelstyle.directive';
import {ChartService} from "./chart.service";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    BodyComponent,
    FirstPageComponent,
    SecondPageComponent,
    CapitalizePipe,
    TranslatePipe,
    FirstPageComp1Component,
    FirstPageComp2Component,
    FirstPageComp3Component,
    FirstPageComp4Component,
    PanelstyleDirective

  ],
  imports: [

    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    ChartsModule,
    AppRoutingModule

  ],
  providers: [ChartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
