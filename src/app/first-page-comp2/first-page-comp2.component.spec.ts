import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstPageComp2Component } from './first-page-comp2.component';

describe('FirstPageComp2Component', () => {
  let component: FirstPageComp2Component;
  let fixture: ComponentFixture<FirstPageComp2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstPageComp2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstPageComp2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
