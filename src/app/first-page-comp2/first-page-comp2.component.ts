import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-first-page-comp2',
  templateUrl: './first-page-comp2.component.html',
  styleUrls: ['./first-page-comp2.component.css']
})
export class FirstPageComp2Component implements OnInit {
  @Input() texto;
  constructor() { }

  ngOnInit() {
  }

}
