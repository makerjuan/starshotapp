import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appPanelstyle]'
})
export class PanelstyleDirective {

  constructor(el: ElementRef) {
    el.nativeElement.classList.add('panelStyle');
  }

}
