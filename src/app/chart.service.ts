import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
@Injectable()
export class ChartService {

  mockdata = [
    [{name: 'Comp A', employees: 10}, {name: 'Comp B', employees: 20}, {name: 'Comp C', employees: 30}, {
      name: 'Comp D',
      employees: 10
    }, {name: 'Comp E', employees: 30}],
    [{name: 'Comp A', employees: 10}, {name: 'Comp B', employees: 20}, {name: 'Comp C', employees: 30}, {
      name: 'Comp D',
      employees: 10
    }, {name: 'Comp E', employees: 30}],
    [{name: 'Comp A', employees: 10}, {name: 'Comp B', employees: 20}, {name: 'Comp C', employees: 30}, {
      name: 'Comp D',
      employees: 10
    }, {name: 'Comp E', employees: 30}],
    []
  ];

  constructor() {
  }

  getIntervalData(seconds: number) {
    return Observable.interval(seconds * 1000).map(() => {
      return this.getRamdomMockData();
    });
  }

  getData() {
    return Observable.create(observable => {
      observable.next(this.getRamdomMockData());
      observable.complete();
    });
  }


  private getRamdomMockData() {
    while(true) {
      let data = this.mockdata[Math.floor(Math.random() * 4) + 1];
      if (data != null)
        return data;
    }
  }
}
