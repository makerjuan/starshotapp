import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ChartService} from '../chart.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {BaseChartDirective} from "ng2-charts";
@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecondPageComponent implements OnInit {
  secondsInterval: number;
  chartData: any;
  pieChartLabels: string[] = [] ;
  pieChartData: number[] = [];
  pieChartType = 'pie';
  subscription: Subscription;
  @ViewChild(BaseChartDirective) public _chart: BaseChartDirective;

  constructor(private chart: ChartService ,private cd : ChangeDetectorRef ) { }

  ngOnInit() {
    this.secondsInterval = 3;
    this.chart.getData().subscribe( res => {
      this.chartData = res;
      console.log(res);
      this.loadChardData();
      this.intervalRequest();
    }).unsubscribe();
  }

  private intervalRequest() {
    this.subscription = this.chart.getIntervalData(this.secondsInterval).subscribe( res => {
      this.chartData = res;
      this.updateChartData();
      console.log(res);
    })
    ;
  }

  private loadChardData() {

    for (let row of this.chartData) {
      this.pieChartData.push(row.employees);
      this.pieChartLabels.push(row.name);

    }
    this.detectChanges();
  }


  private updateChartData(){
    this.pieChartData.splice(0,this.pieChartData.length-1);
    this.pieChartLabels.splice(0,this.pieChartLabels.length-1);
    for (let row of this.chartData) {
      this.pieChartData.push(row.employees);
      this.pieChartLabels.push(row.name);

    }
    setTimeout(() => {
      if (this._chart && this._chart.chart && this._chart.chart.config) {
        this._chart.chart.config.data.labels = this.pieChartLabels;
        this._chart.chart.config.data.data = this.pieChartData;
        this._chart.chart.update();
      }
    },200);
    this.detectChanges();
  }

  detectChanges(){
    if(this.cd != null)
      this.cd.detectChanges();
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  changeInterval() {
    this.subscription.unsubscribe();
    this.intervalRequest();

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
