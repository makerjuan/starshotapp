import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstPageComp1Component } from './first-page-comp1.component';

describe('FirstPageComp1Component', () => {
  let component: FirstPageComp1Component;
  let fixture: ComponentFixture<FirstPageComp1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstPageComp1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstPageComp1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
