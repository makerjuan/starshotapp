import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-first-page-comp1',
  templateUrl: './first-page-comp1.component.html',
  styleUrls: ['./first-page-comp1.component.css']
})
export class FirstPageComp1Component implements OnInit {
  @Input() texto;
  constructor() { }

  ngOnInit() {
  }

}
