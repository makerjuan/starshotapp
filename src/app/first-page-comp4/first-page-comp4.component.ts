import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-first-page-comp4',
  templateUrl: './first-page-comp4.component.html',
  styleUrls: ['./first-page-comp4.component.css']
})
export class FirstPageComp4Component implements OnInit {
  @Input() texto;
  constructor() { }

  ngOnInit() {
  }

}
