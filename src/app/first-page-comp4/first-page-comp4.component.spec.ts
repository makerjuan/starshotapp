import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstPageComp4Component } from './first-page-comp4.component';

describe('FirstPageComp4Component', () => {
  let component: FirstPageComp4Component;
  let fixture: ComponentFixture<FirstPageComp4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstPageComp4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstPageComp4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
