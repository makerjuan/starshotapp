import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(value: string, args?: any): any {
   return value.replace(/\b\w/g, function(l) { return l.toUpperCase(); });
  }

}
