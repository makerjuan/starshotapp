import { Pipe, PipeTransform } from '@angular/core';
import {Observable} from 'rxjs/Observable';


@Pipe({
  name: 'translate',
  pure : false
})
export class TranslatePipe implements PipeTransform {
  private translations: any = {
    'texto1' : 'Este es el texto uno',
    'texto2' : 'Este es el texto dos',
    'texto3' : 'Este es el texto tres',
    'texto4' : 'Este es el texto cuatro'

  };

  constructor() {

  }

  transform(value: any, args?: any): any {
    return this.getTranslation(value);
  }

  /**
   * Simulation of rest response
   * I tried to do with http.get('/assets/translation.json') but it seems that there is an error in angular or in chrome browser that cancelled all request to this file with any reason.
   * @param string
   * @returns {any}
   */
  getTranslation(string): string {

  return  Observable.create( observable => {
     observable.next(this.translations[string]);
   });
    }





}
