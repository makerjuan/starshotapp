import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-first-page-comp3',
  templateUrl: './first-page-comp3.component.html',
  styleUrls: ['./first-page-comp3.component.css']
})
export class FirstPageComp3Component implements OnInit {
  @Input() texto;
  constructor() { }

  ngOnInit() {
  }

}
