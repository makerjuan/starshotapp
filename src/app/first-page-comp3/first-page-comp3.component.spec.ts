import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstPageComp3Component } from './first-page-comp3.component';

describe('FirstPageComp3Component', () => {
  let component: FirstPageComp3Component;
  let fixture: ComponentFixture<FirstPageComp3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstPageComp3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstPageComp3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
