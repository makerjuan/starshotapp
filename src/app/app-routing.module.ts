import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FirstPageComponent} from './first-page/first-page.component';
import {SecondPageComponent} from './second-page/second-page.component';



const routes: Routes = [
  { path: 'firstPage', component: FirstPageComponent},
  {path: 'secondPage', component: SecondPageComponent},
  { path: '',   redirectTo: '/firstPage', pathMatch: 'full' },
  { path: '**', component: FirstPageComponent }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
